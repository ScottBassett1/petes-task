﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json.Serialization;
using Microsoft.VisualBasic;

namespace PetesTask
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to your Contact Book!");
            var loop = true;
            while (loop)
            {
                Console.WriteLine("\nPlease use one of the following commands:\n- Add \n- Delete \n- List \n- Quit");

                string command = Console.ReadLine();

                Contact c = new Contact();

                switch (command)
                {
                    case "List":
                        c.ListContacts();
                        break;
                    case "Add":
                        c.AddContact();
                        break;
                    case "Delete":
                        c.DeleteContact();
                        break;
                    case "Quit":
                        loop = false;
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.WriteLine("\nInvalid command");
                        Console.ResetColor();
                        break;
                }
            }
        }
    }
}
