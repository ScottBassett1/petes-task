﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PetesTask
{
    public class Contact
    {
        public string Name { get; set; }
        public string Number { get; set; }
        public Contact() { }

        //Put this here for the test...
        public Contact(string name, string number)
        {
            name = Name;
            number = Number;
        }

        public void ListContacts()
        {
            using StreamReader r = new StreamReader("C:/Training/PetesTask/Contacts.json");
            string json = r.ReadToEnd();
            var contactList = JsonConvert.DeserializeObject<List<Contact>>(json);

            foreach (var contact in contactList)
            {
                Console.WriteLine($"Name: {contact.Name}, Number: {contact.Number}");
            }
        }
        public void AddContact()
        {
            const string filePath = @"C:\Training\PetesTask\Contacts.json";

            var jsonData = File.ReadAllText(filePath);
            var contactList = JsonConvert.DeserializeObject<List<Contact>>(jsonData) ?? new List<Contact>();

            Console.WriteLine("\nPlease enter new name:");
            string name = Console.ReadLine();

            Console.WriteLine("\nPlease enter new number:");
            string number = Console.ReadLine();

            if (number.Length == 11)
            {
                contactList.Add(new Contact()
                    {
                        Name = name,
                        Number = number
                    }
                );
                jsonData = JsonConvert.SerializeObject(contactList);
                File.WriteAllText(filePath, jsonData);

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("\nContact added!");
                Console.ResetColor();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("\nInvalid number, contact not added");
                Console.ResetColor();
            }
        }

        public void DeleteContact()
        {
            Console.WriteLine("What is the contact's number?");
            string inputNumber = Console.ReadLine();

            var filePath = @"C:\Training\PetesTask\Contacts.json";
            var jsonData = File.ReadAllText(filePath);

            var contactList = JsonConvert.DeserializeObject<List<Contact>>(jsonData) ?? new List<Contact>();
            var tempList = new List<Contact>();

            foreach (var contact in contactList)
            {
                if (contact.Number == inputNumber)
                {
                    tempList.AddRange(contactList);
                    tempList.Remove(contact);
                }
            }

            contactList.Clear();
            contactList.AddRange(tempList);
            tempList.Clear();

            jsonData = JsonConvert.SerializeObject(contactList);
            File.WriteAllText(filePath, jsonData);

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\nContact deleted");
            Console.ResetColor();
        }
    }
}
